﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace ExportOB10Invoices
{
    class Program
    {
        public static string emailHML = string.Empty;
        public static int InvoicesSent = 0;

        static void Main(string[] args)
        {

            Console.WriteLine("Update Start Time...");
            UpdateJobStartTime();

            Console.WriteLine("Creating Text File...");
            CreateTextFile();

            if(InvoicesSent > 0)
            {
                System.Threading.Thread.Sleep(5000); // ensure that the file is no longer in use.
                Console.WriteLine("Exporting Invoices to OB10...");
                ExportInvoicesToOB10();
            }

            Console.WriteLine("Send Notification Emails...");
            SendEmailNotification(emailHML, InvoicesSent);

            Console.WriteLine("Update end time...");
            UpdateJobEndTime();

        }

        private static void ExportInvoicesToOB10()
        {
            var clsRequest = (FtpWebRequest)WebRequest.Create("ftp://34.242.7.134/Invoices.txt");

            clsRequest.Credentials = new NetworkCredential("AAA674235957", "674235957");
            clsRequest.Method = WebRequestMethods.Ftp.UploadFile;

            // Read in file...
            byte[] bFile = File.ReadAllBytes(@"C:\OB10Export\Invoices.txt");

            // upload file...
            Stream clsStream = clsRequest.GetRequestStream();
            clsStream.Write(bFile, 0, bFile.Length);
            clsStream.Close();
            clsStream.Dispose();
        }

        private static void CreateTextFile()
        {
            var lastSuccessfulRunDate = GetLastSuccessfulRunDate();

            List<string> ob10Accounts = GetOB10Accounts();
            string ob10AccountsQueryString = string.Empty;

            foreach (string account in ob10Accounts)
            {
                ob10AccountsQueryString += "'" + account + "',";
            }

            ob10AccountsQueryString = ob10AccountsQueryString.Substring(0, ob10AccountsQueryString.Length - 1);
            string invoiceRef = string.Empty;

            var commandString = new StringBuilder("Select soinv_invref, soinv_invdate, soh_cusref, soh_ordref, soinv_account,");
            commandString.Append("ndm_name, ndm_addr1, ndm_addr2, ndm_addr3, ndm_addr4, ndm_postcode, soh_delname, soh_delad1, soh_delad2, ");
            commandString.Append("soh_delad3, soh_delad4, soh_delpost, soinv_currency, soinv_gdstot,");
            commandString.Append("soinv_vattot, soinv_vrate1, soh_rinvref, soinv_credate, soinv_cretime,  soinv_credate || ' ' || soinv_cretime ");
            commandString.Append("soi_invref, soi_ordref, soitem.soi_product, soitem.soi_custprod, soitem.soi_desc, soitem.soi_per, soitem.soi_itqty, soitem.soi_price,");
            commandString.Append("soitem.soi_invval, soitem.soi_vatcode, soitem.soi_invval, soitem.soi_discper, soitem.soi_per, soitem.soi_seq, soitem.soi_ittype ");
            commandString.Append("FROM soinvoice, soitem, sohead, ndmas ");
            commandString.Append("Where soinv_invref=soi_invref and soinv_ordref=soi_ordref and soi_ordref=soh_ordref and soh_account=ndm_ndcode and ");
            commandString.Append("soinv_ordref <> '' and soinv_credate > '" + lastSuccessfulRunDate.ToString("MM/dd/yyyy") + "' and ");
            commandString.Append("soinv_account IN (" + ob10AccountsQueryString + ") Order by soinv_invref, soi_seq");

            using (var connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["dsnINFORMIXrout"].ConnectionString))
            {
                double goodsTotal = 0;
                double vatTotal = 0;

                var command = new OdbcCommand(commandString.ToString(), connection);
                connection.Open();
                var reader = command.ExecuteReader();
                var recordType = string.Empty;
                var streamWriter = new StreamWriter("C:\\OB10Export\\Invoices.txt");

                while (reader.Read())
                {

                    if (invoiceRef != reader["soinv_invref"].ToString())
                    {

                        if (!string.IsNullOrEmpty(invoiceRef))
                        {
                            streamWriter.WriteLine("C|" + goodsTotal + "|" + vatTotal + "|" + (goodsTotal + vatTotal) + "|");
                        }

                        recordType = reader["soinv_invref"].ToString().Substring(0, 1).Trim();
                        var recordDate = recordType == "C" ? Convert.ToDateTime(reader["soinv_credate"]).ToString("dd/MM/yyyy") : Convert.ToDateTime(reader["soinv_invdate"]).ToString("dd/MM/yyyy").Trim();

                        var currency = reader[17].ToString() == "STER" ? "GBP" : reader[17].ToString().Trim();

                        streamWriter.WriteLine("A|" + recordType + "|" + reader["soinv_invref"].ToString().Trim() + "|" + recordDate + "|" + recordDate + "|" + reader["soh_cusref"].ToString().Trim()
                            + "|" + reader["soh_ordref"].ToString().Trim() + "|" + reader["soinv_account"].ToString().Trim() + "|" + reader["soinv_account"].ToString().Trim() + "|" + reader["ndm_name"].ToString().Trim()
                            + "|" + reader["ndm_addr1"].ToString().Trim() + "|" + reader["ndm_addr2"].ToString().Trim() + "|" + reader["ndm_addr3"].ToString().Trim() + "|" + reader["ndm_addr4"].ToString().Trim()
                            + "|" + reader["ndm_postcode"].ToString().Trim() + "||" + reader["soh_delname"].ToString().Trim() + "|" + reader["soh_delad1"].ToString().Trim() + "|" + reader["soh_delad2"].ToString().Trim()
                            + "|" + reader["soh_delad3"].ToString().Trim() + "|" + reader["soh_delad4"].ToString().Trim() + "|" + reader["soh_delpost"].ToString().Trim() + "| 322 2372 00 |" + currency + " |||"
                            + reader["soh_rinvref"].ToString().Trim() + "|Accounts@routeco.com|Routeco Limited|Davy Avenue|Knowlhill|Milton Keynes|United Kingdom|MK5 8HJ");
       
                        InvoicesSent += 1;

                        

                        emailHML += reader["soinv_account"].ToString().Trim() + ", " + reader["ndm_name"].ToString().Trim() + ", " + reader["soinv_invref"].ToString().Trim() + ", " + recordDate + ", " +
                            reader["soinv_gdstot"].ToString().Trim() + ", " + reader["soinv_vattot"].ToString().Trim() + " <br/>";

                        RecordInvoiceOB10(reader["soinv_account"].ToString().Trim(), reader["ndm_name"].ToString().Trim(), reader["soinv_invref"].ToString().Trim(), Convert.ToDateTime(reader["soinv_credate"]).ToString("yyyy-MM-dd").Trim(), reader["soi_invval"].ToString().Trim());
                    }

                    goodsTotal = Convert.ToDouble(reader["soinv_gdstot"]);
                    vatTotal = Convert.ToDouble(reader["soinv_vattot"]);
                    invoiceRef = reader["soinv_invref"].ToString();

                    streamWriter.WriteLine("B|" + reader["soh_cusref"].ToString().Trim() + "|" + reader["soi_seq"].ToString().Trim() + "||" + reader["soi_product"].ToString().Trim() + "" +
                    (reader["soi_custprod"].ToString().Trim() == reader["soi_product"].ToString().Trim() ? "" : reader["soi_custprod"].ToString().Trim()) + "|" + reader["soi_desc"].ToString().Trim() + "|" +
                    reader["soi_per"].ToString().Trim() + "|" + reader["soi_itqty"].ToString().Trim() + "|" + reader["soi_price"].ToString().Trim() + "|" +
                    (reader["soi_ittype"].ToString().Trim() == "I" && recordType == "C" ? "-" + reader["soi_invval"] : reader["soi_invval"]) + "|" +
                    reader["soinv_vrate1"].ToString().Trim() + "|" + reader["soi_vatcode"].ToString().Trim() + "|" +
                    (Convert.ToDouble(reader["soi_invval"]) / 100) * Convert.ToDouble(reader["soinv_vrate1"]) + "|Standard|" + reader["soi_discper"] + "|" +
                    ((Convert.ToDouble(reader["soi_price"]) * Convert.ToDouble(reader["soi_itqty"]) / Convert.ToDouble(reader["soi_per"])) - Convert.ToDouble(reader["soi_invval"])));


                }

                streamWriter.WriteLine("C|" + goodsTotal + "|" + vatTotal + "|" + (goodsTotal + vatTotal) + "|");
                streamWriter.Flush();
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        private static List<string> GetOB10Accounts()
        {
            string sql = "Select cust from tblob10accounts";
            List<string> ob10Accounts = new List<string>();

            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString))
            {
                var command = new SqlCommand(sql, connection);
                connection.Open();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ob10Accounts.Add(reader["cust"].ToString());
                }

            }

            return ob10Accounts;
        }

        private static void UpdateJobStartTime()
        {
            string sql = "update tbljobmanagement set last_start_datetime = GETDATE(), last_end_datetime = null where job_name = 'mySQL_Export_Invoices_OB10'";

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString))
            {
                var command = new SqlCommand(sql, connection);
                connection.Open();
                command.ExecuteNonQuery();
                command.Dispose();
            }
        }


        private static void UpdateJobEndTime()
        {
            string sql = "update tbljobmanagement set last_end_datetime = GETDATE(), last_successful_run_date = GETDATE() Where job_name = 'mySQL_Export_Invoices_OB10'";

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString))
            {
                var command = new SqlCommand(sql, connection);
                connection.Open();
                command.ExecuteNonQuery();
                command.Dispose();
            }
        }


        private static DateTime GetLastSuccessfulRunDate()
        {
            DateTime lastSuccessfulRunDate;
            string sql = "Select last_successful_run_date FROM tbljobmanagement where job_name = 'mySQL_Export_Invoices_OB10'";

            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString))
            {
                var command = new SqlCommand(sql, connection);
                connection.Open();
                lastSuccessfulRunDate = Convert.ToDateTime(command.ExecuteScalar());
            }

            return lastSuccessfulRunDate;
        }

        private static void SendEmailNotification(string html, int invoicesSent)
        {
            var emailSender = new SmtpClient("smtp.office365.com", 587)
            {
                UseDefaultCredentials = false,
                EnableSsl = true,
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["MailUsername"], ConfigurationManager.AppSettings["MailPassword"])
            };

            emailSender.DeliveryMethod = SmtpDeliveryMethod.Network;

            var email = new MailMessage
            {
                Subject = "Daily Invoices to OB10",
                Body = invoicesSent > 0 ? "Invoices sent to OB10 <br/><br/>" + emailHML : "No invoices sent to OB10",
                IsBodyHtml = true
            };

            var emailAddress = new MailAddress("MIS.Reporting@routeco.com");
            email.From = emailAddress;

            email.To.Add("Richard.Webb@routeco.com");
            email.To.Add("paul.bruniges@routeco.com");
            
            emailSender.Send(email);
        }

        private static void RecordInvoiceOB10(string AccountNumber, string AccountName, string InvoiceRef, string InvoiceDate, string InvoiceValue)
        {
            try
            {
                string sql = "Insert into tblob10_Invoices Values(@InvoiceRef, @AccountNumber, @AccountName, @InvoiceDate, @InvoiceValue)";

                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString))
                {
                    var command = new SqlCommand(sql, connection);
                    command.Parameters.AddWithValue("@AccountNumber", AccountNumber);
                    command.Parameters.AddWithValue("@AccountName", AccountName);
                    command.Parameters.AddWithValue("@InvoiceRef", InvoiceRef);
                    command.Parameters.AddWithValue("@InvoiceDate", InvoiceDate);
                    command.Parameters.AddWithValue("@InvoiceValue", InvoiceValue);

                    connection.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();
                }
            }
            catch
            {
                //Ignore Error
            }


        }


    }
}
